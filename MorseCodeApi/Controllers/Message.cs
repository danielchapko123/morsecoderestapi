﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MorseCodeApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessageController : ControllerBase
    {
        private readonly ILogger<MessageController> _logger;

        public MessageController(ILogger<MessageController> logger)
        {
            _logger = logger;

            //messages.Add("hello");
            //messages.Add("Pizza");
        }

        public ArrayList messages; 
        //gets latest messages
        [HttpGet]
        public IActionResult Get()
        {
            String str = "";
            for (int i = 0; i < messages.Count; i++) 
            {
                str = str + messages[i];
                }
            return Ok(messages);
        }
        
        [HttpPost]
        public IActionResult Send(String message, int length)
        {

            messages.Add("Pizza");
            return Ok();
        }       
    }
}