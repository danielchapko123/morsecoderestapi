import React, { Component } from 'react';

function Message(props) {
    return <li key={props.index}> {props.message}</li>;
}

export class Counter extends Component {
  static displayName = Counter.name;

  constructor(props) {
    super(props);
      this.state = { currentCount: 0, messages: []};
    this.incrementCounter = this.incrementCounter.bind(this);
  }
    async beepButton() {
        console.log("beep")
        const settings = {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        };
        try {
            const fetchResponse = await fetch('message', settings);
            const data = await fetchResponse.json();
            console.log(data);
        } catch (e) {
            return e;
        }  
    }
    incrementCounter() {

        this.getMessages();
        this.setState({
          currentCount: this.state.currentCount + 1
        });
    }

    async getMessages() {
        const response = await fetch('message');
        console.log(response);
        const data = await response.json();

        this.setState({ forecasts: data, messages: data});
    }

    render() {
    const items = []

    for (const [index, value] of this.state.messages.entries()) {
        items.push(<Message index={index} message={value} />)
    }

    return (
      <div>
        <h1>Counter</h1>

        <h1>Messages:</h1>
        <div>{items}</div>
                  
        <p aria-live="polite">Current count: <strong>{this.state.currentCount}</strong></p>

        <button className="btn btn-primary" onClick={this.beepButton}>beep</button>
        <button className="btn btn-primary" onClick={this.incrementCounter}>Increment</button>
      </div>
    );
  }
}